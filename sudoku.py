"""Simple sudoku solver."""

import sys


def construct_grid(p_kind):
    """Construct the problem in buildin."""
    # Easy
    if p_kind == 1:
        r_grid = "000000000"
        r_grid += "000000027"
        r_grid += "400608000"
        r_grid += "071000300"
        r_grid += "238506419"
        r_grid += "964100750"
        r_grid += "395027800"
        r_grid += "182060974"
        r_grid += "046819205"
    # Easy
    elif p_kind == 2:
        r_grid = "003020600"
        r_grid += "900305001"
        r_grid += "001806400"
        r_grid += "008102900"
        r_grid += "700000008"
        r_grid += "006708200"
        r_grid += "002609500"
        r_grid += "800203009"
        r_grid += "005010300"

    # Easy
    elif p_kind == 3:
        r_grid = "000000000"
        r_grid += "000000140"
        r_grid += "500076080"
        r_grid += "012400000"
        r_grid += "000000000"
        r_grid += "000305006"
        r_grid += "000100000"
        r_grid += "600000007"
        r_grid += "003009000"
    # Middel
    elif p_kind == 4:
        r_grid = "000000000"
        r_grid += "000000280"
        r_grid += "376400000"
        r_grid += "700001000"
        r_grid += "020000000"
        r_grid += "400300006"
        r_grid += "010028000"
        r_grid += "000005000"
        r_grid += "000000003"

    #
    elif p_kind == 5:
        r_grid = "000009700"
        r_grid += "004000000"
        r_grid += "002000008"
        r_grid += "000260003"
        r_grid += "091000000"
        r_grid += "070000000"
        r_grid += "000800002"
        r_grid += "010000900"
        r_grid += "008300000"

    # Very hard
    elif p_kind == 6:
        pure_grid = ".....6....59.....82....8..."
        pure_grid += ".45........3........6..3.54"
        pure_grid += "...325..6.................."
        r_grid = pure_grid.replace(".", "0")
    return r_grid


full_list = 0b111111111


def gen_list(p_bit):
    """Generate List."""
    r_list = []
    for i in range(9):
        mask = 1 << i
        if (mask & p_bit) != 0:
            r_list.append(mask)
    return r_list


def count_bit(p_bit):
    """Count the bit give, B."""
    c = 0
    for i in range(9):
        if (p_bit & (1 << i)) != 0:
            c += 1
    return c


def gen_bit_array(p_grid):
    """Generate bit array."""
    ret = []
    for c in p_grid:
        i = int(c)
        ret.append(0 if i == 0 else 1 << (i - 1))
    return ret


def candidate(p_grid, ppoint, debug=False):
    """Generate candidate."""
    (x, y) = ppoint
    now = p_grid[x + y * 9]
    if debug:
        print("now=%s" % now)
    candidate = full_list

    # Line
    line = p_grid[y * 9: y * 9 + 9]
    # Column
    line.extend(p_grid[x:81:9])
    # Block
    x0 = int(x / 3) * 3
    y0 = int(y / 3) * 3
    r = y % 3
    if r != 0:
        line.extend(p_grid[x0 + y0 * 9:x0 + 3 + y0 * 9])
    y0 += 1
    if r != 1:
        line.extend(p_grid[x0 + y0 * 9:x0 + 3 + y0 * 9])
    y0 += 1
    if r != 2:
        line.extend(p_grid[x + y0 * 9:x0 + 3 + y0 * 9])
    m = 0

    for lc in line:
        m |= lc
    candidate &= ~m
    return candidate


def find_bit(b):
    """Find the bit, B."""
    for i in range(0, 9):
        mask = 1 << i
        if (mask & b) != 0:
            return str(i + 1)
    return "0"


def display_grid(p_grid, debug=False):
    """Display the grid."""
    # print(find_bit(grid[15]))
    # print(grid[23])
    for i in range(0, 81, 9):
        if debug:
            print(i)
        # l = grid[i:i+9]
        # print(l[:3], l[3:6], l[6:9])
        bstr = ""
        for j in range(0, 9):
            if debug:
                print("i+j=%d, => %s" % (i + j, bin(p_grid[i + j])))
                print(" find_bit, res:%s" % find_bit(p_grid[i + j]))
            bstr += find_bit(p_grid[i + j])
        print("%s %s %s" % (bstr[:3], bstr[3:6], bstr[6:9]))

        ln = i / 9
        if (ln % 3) == 2:
            print


def is_full(p_grid):
    """Is full in the grid, i.e. is finished."""
    for i in range(0, 81, 9):
        b = 0
        for j in range(0, 9):
            b |= p_grid[i + j]
        if b == 0b111111111:
            continue
        else:
            return False
    return True


def try_put(c_list, t_grid, point, debug):
    """Try to put."""
    [ix, iy] = point
    for cc in c_list:
        # Save
        # print("Index:%d" % (ix+iy*9))
        oc = t_grid[ix + iy * 9]
        # print(" oc=%s" % oc)
        t_grid[ix + iy * 9] = cc
        # print(" new val=%s" % bin(cc))
        if debug:
            print("New grid@(%d,%d)=>%s:" % (ix, iy, cc))
            # print(t_grid)
            display_grid(t_grid)
        ok, t_grid = solver(t_grid, debug)
        if ok:
            return ok, t_grid
        # Restore
        t_grid[ix + iy * 9] = oc
    return False, t_grid


def solver_body(i_grid, a_len, c_list, debug):
    """Solver body routine."""
    for i in range(10):
        if len(a_len[i]) == 0:
            continue
        if debug:
            print("Count=%d:" % i)
        for point in a_len[i]:
            if debug:
                print(" Point:", point)
            xy, clist = point
            [ix, iy] = xy
            cclist = gen_list(clist)
            # print(" Cclist:", cclist)
            is_finished, p_grid = try_put(cclist, i_grid, [ix, iy], debug)
            if is_finished:
                return True, i_grid
            if debug:
                print("False at (%d,%d), clist=%s" % (ix, iy, c_list))
            return False, i_grid
    return False, i_grid


def solver(p_grid, debug=False):
    """Solver main routine."""
    if is_full(p_grid):
        return True, p_grid

    # 0: nothing, 9:full
    alen = [[] for i in range(10)]
    for iy in range(9):
        for ix in range(9):
            if p_grid[iy * 9 + ix] != 0:
                continue
            clist = candidate(p_grid, (ix, iy), debug)
            if debug:
                print("@(%d,%d) clist :%s" % (ix, iy, bin(clist)))
            alen[count_bit(clist)].append(([ix, iy], clist))

    return solver_body(p_grid, alen, clist, debug)


if __name__ == "__main__":

    args = sys.argv
    argc = len(args)
    if False:
        print(args)
        print(argc)
    if argc != 2:
        print("Usage: python t53.py ID(1-6)")
        sys.exit(-1)
    kind = int(args[1])

    print(" ID: [%d]" % kind)
    grid = construct_grid(kind)
    print(" Input:")
    bgrid = gen_bit_array(grid)
    # print(bgrid)
    display_grid(bgrid, False)

    # print(is_full(bgrid))
    status, rgrid = solver(bgrid, False)
    print(" =>Result:")
    if status:
        display_grid(rgrid)
